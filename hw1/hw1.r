#!/usr/bin/Rscript
#gtusername: rmartinez6
options(expressions=500000)

#' 2. Log Gamma (Loop)
#'
#' This function computes the log gamma function using a loop.
#' @param value to compute log gamma.
#' @examples
#' log_gamma_loop(5) = 3.178054
log_gamma_loop <- function(n){
  total = 0
  if(n == 1){
    return(0)
  }
  for (i in 2:n){
    total = total + log(i-1)
  }
  return (total)
}

#' 3. Log Gamma (Recursive)
#'
#' This function computes the log gamma function using recursion.
#' @param value to compute log gamma.
#' @examples
#' log_gamma_recursive(5) = 3.178054
log_gamma_recursive <- function(n){
  total = 0
  if(n <= 2){
    return(0)
  }else{
    return(log(n-1) + log_gamma_recursive(n-1))
  }
}



#' 4. Sum of Log Gamma
#'
#' This function computes sum of the input value.
#' the first uses the loop technique while the other uses recursion
#' @param value to compute the sum of log gamma.
#' @examples
#' sum_log_gamma_loop(5) = 5.66296
#' sum_log_gamma_recursive(5) = 5.66296

sum_log_gamma_loop <- function(n){
  sum = 0
  for(i in 1:n){
    if(i == 1){
      sum = sum + 0
    }else{
      sum = sum + log_gamma_loop(i)
    }
  }
  return(sum)
}

sum_log_gamma_recursive <- function(n){
  sum = 0
  # print(n)
  for(i in 1:n){
    sum = sum + log_gamma_recursive(i)
  }
  return(sum)
}


# 5. Compare Results to Built-In R Function
# Here you will be able to see the total amount of time needed to finish the loop
# for each function


#' This function uses the Built-In R function lgamma.
#' the first uses the loop technique while the other uses recursion
#' @param value to compute the sum of log gamma.
#' @examples
#' sum_lgamma(5) = 5.66296
sum_lgamma <- function(n) {
  if (n < 1) {
    stop("value out of range")
  }
  sum <- 0
  for (i in seq(1, n, 1)) {
    sum <- sum + lgamma(i)
  }
  return(sum)
}


# [1] "Loop Function:"
#    user  system elapsed
#   8.040   0.041   8.102
# [1] "Recursive Function"
#    user  system elapsed
#  25.467   0.116  25.645
# [1] "Built-In R Function"
#    user  system elapsed
#  0.01    0.00    0.01

a = seq(1, 2300, 100);
print('Loop Function:')
system.time(for (e in a) sum_log_gamma_loop(e))
print('Recursive Function')
system.time(for (e in a) sum_log_gamma_recursive(e))
print('Built-In R Function')
system.time(for (e in a) sum_lgamma(e))


######################################################################
## used to generate csv file in helping with table and chart creation
# x <- numeric(length(a))
# y <- character(length(a))
#
# for(e in a){
#   x[e] <- e
#   y[e] <- system.time(sum_log_gamma_loop(e))[3]
# }
# df_loop = data.frame(interval=na.omit(x),loop= na.omit(y), stringsAsFactors=FALSE)
#
# for(e in a){
#   x[e] <- e
#   y[e] <- system.time(sum_lgamma(e))[3]
# }
# df_r = data.frame(interval=na.omit(x), r = na.omit(y), stringsAsFactors=FALSE)
#
# for(e in a){
#   x[e] <- e
#   y[e] <- system.time(sum_log_gamma_recursive(e))[3]
# }
# df_recursion = data.frame(interval=na.omit(x), recursion = na.omit(y), stringsAsFactors=FALSE)
#
#
# df <- Reduce(function(x, y) merge(x, y, all=TRUE,by=c("interval")), list(df_loop, df_r, df_recursion))
# write.table(df, file='hw1_result.csv',sep = ",", col.names = NA,)
